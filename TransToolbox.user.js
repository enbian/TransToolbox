// ==UserScript==
// @name        Trans Toolbox (UNIL)
// @namespace   https://codeberg.org/enbian/TransToolbox
// @version     2.2.0
// @license     Unlicense
// @author      enbian
// @description Script Greasemonkey qui permet de changer son prénom et son genre sur MyUNIL et Moodle (UNIL).
// @icon        https://codeberg.org/repo-avatars/a06f06b5a14e1210e851f956ff265c310ef555299b6f272f9287c932a3d13dda
// @homepageURL https://codeberg.org/enbian/TransToolbox
// @supportURL  https://codeberg.org/enbian/TransToolbox/issues
// @downloadURL https://codeberg.org/enbian/TransToolbox/raw/branch/main/TransToolbox.user.js
// @include     https://moodle.unil.ch/*
// @include     https://my.unil.ch/*
// @include     https://renouvaud1.primo.exlibrisgroup.com/*
// @grant       GM.getValue
// @grant       GM.setValue
// @run-at      document-start
// ==/UserScript==

function getGenderIcon(gd) {
  // Get correct gendered icon class based on user configuration
  const iconMan = '.fu-general-45-o';
  const iconWoman = '.fu-general-44-o';
  const iconEnby = '.fu-general-46-o';
  if (gd.toUpperCase() === 'H') {
    return iconMan;
  } else if (gd.toUpperCase() === 'F') {
    return iconWoman;
  } else if (gd.toUpperCase() === 'X') {
    return iconEnby;
  } else {
    return '';
  }
}

function changeGender() {
  // Change gendered icon
  const queryList = [
    getGenderIcon('H'),
    getGenderIcon('F'),
    getGenderIcon('X'),
  ];
  for (const query of queryList) {
    for (const element of document.querySelectorAll(query)) {
      const genderIcon = getGenderIcon(gender);
      if (genderIcon === '') {
        element.parentElement.parentElement.parentElement.style.display =
          'none';
      } else {
        element.classList.replace(query.substring(1), genderIcon.substring(1));
      }
    }
  }
}

function changeDocumentTitle() {
  // Change document title
  document.title = document.title.replace(deadname, realname);
}

function hideElement(queryList) {
  // Hide elements that match queries
  for (const query of queryList) {
    for (const element of document.querySelectorAll(query)) {
      element.style.display = 'none';
    }
  }
}

function changeInitials(queryList) {
  // Change initials in elements that match queries
  for (const query of queryList) {
    for (const element of document.querySelectorAll(query)) {
      element.innerText = element.innerText.replace(deadname[0], realname[0]);
    }
  }
}

function changeName(queryList) {
  // Change name in elements that match queries
  for (const query of queryList) {
    for (const element of document.querySelectorAll(query)) {
      element.innerText = element.innerText.replace(deadname, realname);
    }
  }
}

function changeValue(queryList) {
  // Change name in value of elements that march queries
  for (const query of queryList) {
    for (const element of document.querySelectorAll(query)) {
      try {
        element.value = element.value.replace(deadname, realname);
      } catch (SyntaxError) {}
    }
  }
}

function logTime() {
  if (!loaded) {
    console.info('TTBox loaded data after ' + 5 * count + ' ms.');
  } else {
    console.info(
      'TTBox finished injecting content after ' + 5 * count + ' ms.'
    );
  }
}

function moodleUpdate() {
  // Apply changes to Moodle UNIL (Moodle version 4)
  changeDocumentTitle();
  changeInitials(['span.userinitials']);
  changeValue(['#id_firstname']);
  changeName([
    '.page-header-headings > .h2',
    '.user-heading.d-flex.align-items-center > .d-flex.ml-2 > h2 > a.h4.m-0',
    '#page-course-user .breadcrumb-item:nth-child(5) > a',
    "#page-user-edit #region-main div[role='main'] h2:first-of-type",
  ]);
  hideElement([
    '#fitem_id_email',
    '.profile_tree .node_category.card.d-inline-block.w-100.mb-3:first-of-type li.contentnode',
  ]);
}

function myUnilUpdate() {
  // Apply changes to MyUNIL
  changeName([
    '.dropdown-header > span.label-lg',
    '.z-hlayout div:nth-child(2) .fu-blue.h4.z-label',
    'span.h2.z-label:nth-child(1)',
  ]);
  hideElement(['.h5.fu-blue.z-label']);
  changeGender();
}

function renouvaudUpdate() {
  // Apply changes to Renouvaud
  changeName([
    '.user-name',
    '.user-menu-header > div.layout-column.flex > span.bold-text',
    'span.ng-binding',
  ]);
}

function initConfig() {
  // Initial user configuration
  const deadnameConfig = prompt('Entrez votre deadname');
  const realnameConfig = prompt("Entrez votre prénom d'usage");
  const genderConfig = prompt(
    'Entrez votre genre (F/H/X) - ou bien laissez vide pour masquer le genre'
  );
  if (deadnameConfig !== '' && realnameConfig !== '') {
    // eslint-disable-next-line no-undef
    GM.setValue('deadname', deadnameConfig);
    // eslint-disable-next-line no-undef
    GM.setValue('realname', realnameConfig);
    // eslint-disable-next-line no-undef
    GM.setValue('gender', genderConfig);
    alert('Configuration appliquée avec succès.');
    location.reload();
  } else {
    alert(
      "Erreur: les valeurs nulles ne sont pas autorisées pour le deadname et le prénom d'usage"
    );
  }
}

function launchInject(fn) {
  // Launch injection function
  clearInterval(interval);
  fn();
  logTime();
}

function main() {
  // Entry point function
  count += 1;
  if (
    typeof deadname !== 'undefined' &&
    deadname !== '' &&
    typeof realname !== 'undefined' &&
    realname !== '' &&
    typeof gender !== 'undefined'
  ) {
    if (!loaded) {
      logTime();
      loaded = true;
    }
    if (location.hostname === 'moodle.unil.ch') {
      // If we are on Moodle UNIL, apply changes
      moodleUpdate();
      window.onload = function () {
        launchInject(moodleUpdate);
      };
    } else if (location.hostname === 'my.unil.ch') {
      // If we are on MyUNIL, apply changes so that we don't get deadnamed while the page is loading
      myUnilUpdate();
      // NOTE: after loading, MyUNIL overwrites modified HTML content
      // we need to apply the changes again
      window.onload = function () {
        launchInject(myUnilUpdate);
        setInterval(myUnilUpdate, 10);
      };
    } else if (location.hostname === 'renouvaud1.primo.exlibrisgroup.com') {
      // If we are on Renouvaud, apply changes
      renouvaudUpdate();
      // NOTE: Renouvaud being a horrible single-page app
      // we need to start an infinite loop that calls the function in the background
      window.onload = function () {
        launchInject(renouvaudUpdate);
        setInterval(renouvaudUpdate, 10);
      };
    }
  } else if (count >= 1000) {
    launchInject(initConfig);
  }
}

let count = 0;
let loaded = false;
let deadname = '';
// eslint-disable-next-line no-undef
GM.getValue('deadname').then(function (value) {
  deadname = value;
});
let realname = '';
// eslint-disable-next-line no-undef
GM.getValue('realname').then(function (value) {
  realname = value;
});
let gender = '';
// eslint-disable-next-line no-undef
GM.getValue('gender').then(function (value) {
  gender = value;
});
const interval = setInterval(main, 5);
