# Trans Toolbox

Script Greasemonkey qui permet de changer son prénom et son genre sur MyUNIL et Moodle (UNIL).

## Comment installer?

1. Télécharger l'extension de navigateur appelée _Violentmonkey_:
    - Pour Google Chrome: [c'est par ici](https://chromewebstore.google.com/detail/violentmonkey/jinjaccalgkegednnccohejagnlnfdag)
    - Pour Firefox: [c'est par ici](https://addons.mozilla.org/en-US/firefox/addon/violentmonkey/)
2. Ensuite, [cliquer ici](https://codeberg.org/enbian/TransToolbox/raw/branch/main/TransToolbox.user.js) pour installer le script.
3. Visiter Moodle UNIL ou MyUNIL.
4. Attendre quelques secondes.
5. Lorsqu'un dialogue s'ouvre, entrer les informations demandées.
6. C'est tout bon.

## Fonctionnalités

* Remplace le deadname avec le prénom d'usage sur Moodle et MyUNIL.
* Remplace les initiales dans l'icône du profil Moodle.
* Remplace ou masque (à choix) l'icône genrée sur le profil MyUNIL.
* Ne peut pas modifier l'adresse email, mais cache les endroits où celle-ci est normalement visible pour l'utilisateur·ice·x.

**IMPORTANT:** toutes ces modifications ont lieu uniquement localement. Une connexion depuis un autre appareil affichera les informations qui sont utilisées par l'Université.

## J'ai mal entré mes informations

Si tu as fait une erreur de saisie, tu peux facilement modifier les informations:

1. Clique sur l'extension _Violentmonkey_, et dans celle-ci sur l'icône paramètres.
2. Sur le script _Change My Name_, clique sur le bouton `modifier`/`éditer`.
3. Va sous l'onglet `valeurs`.
4. Clique sur la valeur qui est entrée incorrectement et corrige-la (en prenant soin de laisser les guillemets).

## Des questions/suggestions?

Vous pouvez m'écrire à : `contact <at> enbian.space`.
